extends Control


var save_path = "user://string_orders.json"

var save_file = File.new()
var save_buffer: Dictionary = {}


func _ready():
	randomize()


func _process(_delta):
	_process_string_validity()


func _process_string_validity():
	if $DialogSSO.get_sso_count() < 1:
		$ButtonTranslate.disabled = true
		$ButtonTranslateReverse.disabled = true
	else:
		$ButtonTranslate.disabled = false
		$ButtonTranslateReverse.disabled = false


func create_random_shuffled_string_order_from_original(os_order: String) -> String:
	var os_order_array = []
	
	for letter in os_order:
		os_order_array.append(letter)
	
	os_order_array.shuffle()
	
	var new_string: String
	
	for letter in os_order_array:
		new_string += letter
		
	return new_string


func convert_string(os_order: String, sso_array: Array, content: String, is_reverse: bool = false) -> String:
	var new_string: String
	
	var sso_count = sso_array.size()
	
	if not is_reverse:
		var current_character_index = 0
		
		for letter in content:
			if os_order.find(letter) == -1:
				new_string += letter
			else:
				new_string += sso_array[current_character_index % sso_count][os_order.find(letter)]
				
			current_character_index += 1

	else:
#		if sso_array.size() == 1:
#			for letter in content:
#				if sso_array[0].find(letter) == -1:
#					# letter not found in original string order
#					new_string += letter
#				else:
#					new_string += os_order[sso_array[0].find(letter)]
#		else:
		var current_character_index = 0

		for letter in content:
			if sso_array[current_character_index % sso_count].find(letter) == -1:
				new_string += letter
			else:
				new_string += os_order[sso_array[current_character_index % sso_count].find(letter)]
				
			current_character_index += 1

	return new_string


func _on_ButtonTranslate_button_down():
	$MainText.text = convert_string($OriginStringOrder.text, $DialogSSO.get_sso_array(), $MainText.text, false)
	

func _on_ButtonTranslateReverse_button_down():
	$MainText.text = convert_string($OriginStringOrder.text, $DialogSSO.get_sso_array(), $MainText.text, true)
	

func _on_ButtonEditSSO_button_down():
	$DialogSSO.popup_centered()
