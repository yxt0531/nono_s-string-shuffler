extends WindowDialog


var sso_count: int = 7
var sso_array: Array
	

func _process(_delta):
	_process_sso_count_display()
	_process_sso_text_validity()
	

func _process_sso_count_display():
	$SSOCount.text = str(sso_count)


func _process_sso_text_validity():
	if $TextSSO.readonly:
		$ButtonAPSSO.disabled = true
	elif $TextSSO.text.length() % (get_parent().get_node("OriginStringOrder").text.length() + 1) == 0 and $TextSSO.text.length() > get_parent().get_node("OriginStringOrder").text.length():
		$ButtonAPSSO.disabled = false
	else:
		$ButtonAPSSO.disabled = true
		

func _on_ButtonSSOCIncrease_button_down():
	sso_count += 1
	_on_ButtonGRSSO_button_down()


func _on_ButtonSSOCDecrease_button_down():
	if sso_count > 1:
		sso_count -= 1
		_on_ButtonGRSSO_button_down()


func _on_ButtonGRSSO_button_down():
	$TextSSO.text = ""
	sso_array.clear()
	
	var remaining_sso_count = sso_count
	while remaining_sso_count > 0:
		sso_array.append(get_parent().create_random_shuffled_string_order_from_original(get_parent().get_node("OriginStringOrder").text))
		
		remaining_sso_count -= 1
		
	for sso in sso_array:
		$TextSSO.text += sso + "\n"
	
	$TextSSO.readonly = false


func _on_ButtonAPSSO_button_down():
	sso_array.clear()
	
	var sso_string_text: String = $TextSSO.text
	var sso_string_count: int = sso_string_text.count("\n")
	var current_sso_index: int = 0
	
	while sso_string_count > 0:
		sso_array.append(sso_string_text.substr(current_sso_index * (get_parent().get_node("OriginStringOrder").text.length() + 1), get_parent().get_node("OriginStringOrder").text.length()).replace("\n", ""))
		
		sso_string_count -= 1
		current_sso_index += 1
		
	print(sso_array)
	$TextSSO.readonly = true


func get_sso_count() -> int:
	return sso_array.size()


func get_sso_array() -> Array:
	return sso_array.duplicate(true)
